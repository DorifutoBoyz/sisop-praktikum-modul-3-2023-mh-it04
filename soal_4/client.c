#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

int main() {
    int socket_client;
    char pesan[1024];

    socket_client = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_client == -1) {
        perror("Gagal membuat socket.");
        exit(1);
    }

    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(8080);
    server_address.sin_addr.s_addr = INADDR_ANY;

    if (connect(socket_client, (struct sockaddr*)&server_address, sizeof(server_address)) == -1) {
        perror("Gagal menghubungkan.");
        exit(1);
    }

    while (1) {
        printf("Masukkan pesan: ");
        fgets(pesan, sizeof(pesan), stdin);
        send(socket_client, pesan, strlen(pesan), 0);
    }

    close(socket_client);
    return 0;
}

