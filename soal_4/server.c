#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

int main() {
    int socket_server, socket_client;
    char pesan[1024];
    int jumlah_client = 0;

    socket_server = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_server == -1) {
        perror("Gagal membuat socket.");
        exit(1);
    }

    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(8080);
    server_address.sin_addr.s_addr = INADDR_ANY;

    if (bind(socket_server, (struct sockaddr*)&server_address, sizeof(server_address)) == -1) {
        perror("Koneksi gagal");
        exit(1);
    }

    if (listen(socket_server, 5) == -1) {
        perror("Gagal menghubungkan");
        exit(1);
    }

    printf("Server sedang menunggu koneksi...\n");

    struct sockaddr_in client_address;
    socklen_t client_address_len = sizeof(client_address);

    while (jumlah_client < 5) {
        socket_client = accept(socket_server, (struct sockaddr*)&client_address, &client_address_len);
        jumlah_client++;

        if (socket_client == -1) {
            perror("Gagal Menerima");
            exit(1);
        }

        printf("Terhubung dengan client %d\n", jumlah_client);

        while (1) {
            int koneksi = recv(socket_client, pesan, sizeof(pesan), 0);
            if (koneksi == -1) {
                perror("Gagal Menerima");
                exit(1);
            }

            if (koneksi == 0) {
                printf("Client %d telah menutup koneksi.\n", jumlah_client);
                break;
            }

            pesan[koneksi] = '\0';
            printf("Client %d: %s\n", jumlah_client, pesan);
        }

        close(socket_client);
    }

    close(socket_server);
    return 0;
}

