#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <time.h>

#define baris 4
#define kolom 4

int faktorial(int n) {
    if (n <= 1) {
        return 1;
    }
    if (n < 0) {
        return 0;
    }
    return n * faktorial(n - 1);
}


struct ThreadData {
    int (*matriks)[kolom];
    int (*hasil)[kolom];
};

void *computeFactorial(void *arg) {
    struct ThreadData *data = (struct ThreadData *)arg;
    int (*matriks)[kolom] = data->matriks;
    int (*hasil)[kolom] = data->hasil;

    for (int i = 0; i < baris; i++) {
        for (int j = 0; j < kolom; j++) {
            hasil[i][j] = faktorial(matriks[i][j]);
        }
    }

    pthread_exit(NULL);
}

int main() {
    clock_t start, end;
    double cpu_time_used;

    key_t key = ftok("shared_memory_key", 65);

    int shmid = shmget(key, sizeof(int[baris][kolom]), 0666 | IPC_CREAT);

    if (shmid == -1) {
        perror("shmget");
        exit(1);
    }

    int (*data)[kolom] = shmat(shmid, (void*)0, 0);

    int transposed[kolom][baris];

    for (int i = 0; i < baris; i++) {
        for (int j = 0; j < kolom; j++) {
            transposed[j][i] = data[i][j];
        }
    }

    printf("Hasil Transpose Matriks:\n");
    for (int i = 0; i < kolom; i++) {
        for (int j = 0; j < baris; j++) {
            printf("%d ", transposed[i][j]);
        }
        printf("\n");
    }

    int hasilFaktorial[kolom][baris];

    struct ThreadData threadData;
    threadData.matriks = transposed;
    threadData.hasil = hasilFaktorial;

    start = clock();

    pthread_t tid;

    pthread_create(&tid, NULL, computeFactorial, &threadData);

    pthread_join(tid, NULL);

    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

    printf("Matriks Hasil Faktorial:\n");
    for (int i = 0; i < kolom; i++) {
        for (int j = 0; j < baris; j++) {
            printf("%d ", hasilFaktorial[i][j]);
    }
        printf("\n");
    }

    printf("Waktu eksekusi: %f detik\n", cpu_time_used);

    shmdt(data);

    return 0;
}

