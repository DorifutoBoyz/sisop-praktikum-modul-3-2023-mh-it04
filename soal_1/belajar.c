#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>

int main() {

    int matriks1[4][2];
    int matriks2[2][4];

    srand(time(0));

    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 2; j++) {
            matriks1[i][j] = rand() % 4 + 1;
        }
    }

    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 4; j++) {
            matriks2[i][j] = rand() % 5 + 1;
        }
    }

    printf("Matriks Pertama:\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 2; j++) {
            printf("%d\t", matriks1[i][j]);
        }
        printf("\n");
    }

    printf("Matriks Kedua:\n");
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 4; j++) {
            printf("%d\t", matriks2[i][j]);
        }
        printf("\n");
    }

    int hasil[4][4] = {0};
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            for (int k = 0; k < 2; k++) {
                hasil[i][j] += matriks1[i][k] * matriks2[k][j];
            }
        }
    }

    printf("Hasil Perkalian Matriks:\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            printf("%d\t", hasil[i][j]);
        }
        printf("\n");
    }

    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            hasil[i][j] -= 1;
        }
    }

    printf("Hasil Perkalian Matriks (setelah dikurangi 1):\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            printf("%d\t", hasil[i][j]);
        }
        printf("\n");
    }

    key_t key = ftok("shared_memory_key", 65);
    int shmid = shmget(key, sizeof(int[4][4]), 0666 | IPC_CREAT);
    int (*shared_memory_data)[4] = shmat(shmid, (void *)0, 0);

    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            shared_memory_data[i][j] = hasil[i][j];
        }
    }

    shmdt(shared_memory_data);

    return 0;
}

