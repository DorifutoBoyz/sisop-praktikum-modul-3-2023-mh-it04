#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

#define baris 4
#define kolom 4

int faktorial(int n) {
    if (n <= 1) {
        return 1;
    }
    if (n < 0) {
        return 0; 
    }
    return n * faktorial(n - 1);
}

int main() {

    key_t key = ftok("shared_memory_key", 65);

    int shmid = shmget(key, sizeof(int[baris][kolom]), 0666 | IPC_CREAT);

    if (shmid == -1) {
        perror("shmget");
        exit(1);
    }

    int (*data)[kolom] = shmat(shmid, (void*)0, 0);

    int transposed[kolom][baris];

    for (int i = 0; i < baris; i++) {
        for (int j = 0; j < kolom; j++) {
            transposed[j][i] = data[i][j];
        }
    }

    printf("Hasil Transpose Matriks:\n");
    for (int i = 0; i < kolom; i++) {
        for (int j = 0; j < baris; j++) {
            printf("%d ", transposed[i][j]);
        }
        printf("\n");
    }

    int hasilFaktorial[kolom][baris];

    clock_t start = clock();

    for (int i = 0; i < kolom; i++) {
        for (int j = 0; j < baris; j++) {
            hasilFaktorial[i][j] = faktorial(transposed[i][j]);
        }
    }

    clock_t end = clock();

    printf("Matriks Hasil Faktorial:\n");
    for (int i = 0; i < kolom; i++) {
        for (int j = 0; j < baris; j++) {
            printf("%d ", hasilFaktorial[i][j]);
        }
        printf("\n");
    }

    shmdt(data);

    double time_taken = ((double)(end - start)) / CLOCKS_PER_SEC;
    printf("Waktu eksekusi: %f detik\n", time_taken);

    return 0;
}

