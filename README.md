# sisop-praktikum-modul-3-2023-MH-IT04
DRAFT LAPORAN resmi

**Kelompok IT-04**
| Nama | NRP |
| ------ | ------ |
| Marselinus Krisnawan Riandika | 5027221056 |
| Aras Rizky Ananta |	5027221053 |
| Atha Rahma Arianti |	5027221030 |

## Nomor 1
**Soal:**

Epul memiliki seorang adik SMA bernama Azka. Azka bersekolah di Thursina Malang. Di sekolah, Azka diperkenalkan oleh adanya kata matriks. Karena Azka baru pertama kali mendengar kata matriks, Azka meminta tolong kepada kakaknya Epul untuk belajar matriks.  Karena Epul adalah seorang kakak yang baik, Epul memiliki ide untuk membuat program yang bisa membantu adik tercintanya, tetapi dia masih bingung untuk membuatnya. Karena Epul adalah bagian dari IT05 dan praktikan sisop, sebagai warga IT05 yang baik, bantu Epul mengerjakan programnya dengan ketentuan sebagai berikut :

Membuat program C dengan nama belajar.c, yang berisi program untuk melakukan perkalian matriks. Agar ukuran matriks bervariasi, maka ukuran matriks pertama adalah [4]×2 dan matriks kedua 2×[4]. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-4 (inklusif), dan rentang pada matriks kedua adalah 1-5 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar. 

Karena Epul merasa ada yang kurang, Epul meminta agar hasil dari perkalian tersebut dikurang 1 di setiap matriks.
Karena Epul baru belajar modul 3, Epul ngide ingin meminta agar menerapkan konsep shared memory. Buatlah program C kedua dengan nama yang.c. Program ini akan mengambil variabel hasil pengurangan dari perkalian matriks dari program belajar.c (program sebelumnya). Hasil dari pengurangan perkalian matriks tersebut dilakukan transpose matriks dan diperlihatkan hasilnya. 
(Catatan: wajib menerapkan konsep shared memory)
Setelah ditampilkan, Epul pengen adiknya belajar lebih, sehingga untuk setiap angka dari transpose matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.
(Catatan: Wajib menerapkan thread dan multithreading dalam penghitungan faktorial)

Epul penasaran mengapa dibuat thread dan multithreading pada program sebelumnya, dengan demikian dibuatlah program C ketiga dengan nama rajin.c. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada yang.c namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak. 
Dokumentasikan dan sampaikan saat demo dan laporan resmi.

Catatan:
Key memory diambil berdasarkan program dari belajar.c sehingga program belajar.c tidak perlu menghasilkan key memory (value). Dengan demikian, pada program yang.c dan rajin.c hanya perlu mengambil key memory dari belajar.c

Untuk kelompok 1 - 9, 11 - 19, 21 menggunakan  digit akhir saja.
	Contoh : 19 -> 9

Untuk kelompok 10 dan 20 menambahkan digit awal dan akhir.
			Contoh : 20 - > 2

**Pengerjaan:**

Berikut merupakan isi dari file belajar.c:

	#include <stdio.h>
	#include <stdlib.h>
	#include <time.h>
	#include <sys/ipc.h>
	#include <sys/shm.h>

	int main() {

		int matriks1[4][2];
		int matriks2[2][4];

		srand(time(0));

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 2; j++) {
				matriks1[i][j] = rand() % 4 + 1;
			}
		}

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 4; j++) {
				matriks2[i][j] = rand() % 5 + 1;
			}
		}

		printf("Matriks Pertama:\n");
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 2; j++) {
				printf("%d\t", matriks1[i][j]);
			}
			printf("\n");
		}

		printf("Matriks Kedua:\n");
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 4; j++) {
				printf("%d\t", matriks2[i][j]);
			}
			printf("\n");
		}

		int hasil[4][4] = {0};
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				for (int k = 0; k < 2; k++) {
					hasil[i][j] += matriks1[i][k] * matriks2[k][j];
				}
			}
		}

		printf("Hasil Perkalian Matriks:\n");
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				printf("%d\t", hasil[i][j]);
			}
			printf("\n");
		}

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				hasil[i][j] -= 1;
			}
		}

		printf("Hasil Perkalian Matriks (setelah dikurangi 1):\n");
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				printf("%d\t", hasil[i][j]);
			}
			printf("\n");
		}

		key_t key = ftok("shared_memory_key", 65);
		int shmid = shmget(key, sizeof(int[4][4]), 0666 | IPC_CREAT);
		int (*shared_memory_data)[4] = shmat(shmid, (void *)0, 0);

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				shared_memory_data[i][j] = hasil[i][j];
			}
		}

		shmdt(shared_memory_data);

		return 0;
	}



belajar.c berguna untuk menghasilkan dua matriks dengan angka random, yang kemudian kedua matriks tersebut dikalikan satu sama lain, dan hasil perkaliannya (tiap elemen) dikurangi 1. Hasil perhitungannya disimpan di dalam shared memory. Matriks diisi dengan angka random menggunakan `rand()`. Hasil perkalian kedua matriks disimpan dalam matriks `hasil`. Program membuat shared memory menggunakan `ftok()` untuk mendapatkan key, kemudian menggunakan `shmget()` untuk mendapatkan ID shared memory. Hasil perkalian yang sudah dikurangi 1 kemudian disalin ke shared memory. Terakhir, program melepaskan shared memory menggunakan `shmdt()`.

Berikut adalah isi dari file yang.c:

	#include <stdio.h>
	#include <stdlib.h>
	#include <sys/ipc.h>
	#include <sys/shm.h>
	#include <pthread.h>
	#include <time.h>

	#define baris 4
	#define kolom 4

	int faktorial(int n) {
		if (n <= 1) {
			return 1;
		}
		if (n < 0) {
			return 0;
		}
		return n * faktorial(n - 1);
	}


	struct ThreadData {
		int (*matriks)[kolom];
		int (*hasil)[kolom];
	};

	void *computeFactorial(void *arg) {
		struct ThreadData *data = (struct ThreadData *)arg;
		int (*matriks)[kolom] = data->matriks;
		int (*hasil)[kolom] = data->hasil;

		for (int i = 0; i < baris; i++) {
			for (int j = 0; j < kolom; j++) {
				hasil[i][j] = faktorial(matriks[i][j]);
			}
		}

		pthread_exit(NULL);
	}

	int main() {
		clock_t start, end;
		double cpu_time_used;

		key_t key = ftok("shared_memory_key", 65);

		int shmid = shmget(key, sizeof(int[baris][kolom]), 0666 | IPC_CREAT);

		if (shmid == -1) {
			perror("shmget");
			exit(1);
		}

		int (*data)[kolom] = shmat(shmid, (void*)0, 0);

		int transposed[kolom][baris];

		for (int i = 0; i < baris; i++) {
			for (int j = 0; j < kolom; j++) {
				transposed[j][i] = data[i][j];
			}
		}

		printf("Hasil Transpose Matriks:\n");
		for (int i = 0; i < kolom; i++) {
			for (int j = 0; j < baris; j++) {
				printf("%d ", transposed[i][j]);
			}
			printf("\n");
		}

		int hasilFaktorial[kolom][baris];

		struct ThreadData threadData;
		threadData.matriks = transposed;
		threadData.hasil = hasilFaktorial;

		start = clock();

		pthread_t tid;

		pthread_create(&tid, NULL, computeFactorial, &threadData);

		pthread_join(tid, NULL);

		end = clock();
		cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

		printf("Matriks Hasil Faktorial:\n");
		for (int i = 0; i < kolom; i++) {
			for (int j = 0; j < baris; j++) {
				printf("%d ", hasilFaktorial[i][j]);
		}
			printf("\n");
		}

		printf("Waktu eksekusi: %f detik\n", cpu_time_used);

		shmdt(data);

		return 0;
	}

yang.c mengambil hasil dari belajar.c yang disimpan dalam shared memory, kemudian me-transpose matriksnya dan difaktorialkan. Hasil transpose dari matriks tersebut disimpan dalam matriks `transposed`, lalu setiap elemennya difaktorialkan. 

Durasi eksekusi program diukur menggunakan `clock()` saat sebelum dan sesudah perhitungan faktorial dilakukan. Hasil faktorial disimpan dalam matriks `hasilFaktorial`, yang kemudian program mencetak hasil faktorial serta waktu eksekusinya. Terakhir, program melepaskan shared memory menggunakan `shmdt()`.

Berikut adalah isi dari file rajin.c:

	#include <stdio.h>
	#include <stdlib.h>
	#include <sys/ipc.h>
	#include <sys/shm.h>
	#include <time.h>

	#define baris 4
	#define kolom 4

	int faktorial(int n) {
		if (n <= 1) {
			return 1;
		}
		if (n < 0) {
			return 0; 
		}
		return n * faktorial(n - 1);
	}

	int main() {

		key_t key = ftok("shared_memory_key", 65);

		int shmid = shmget(key, sizeof(int[baris][kolom]), 0666 | IPC_CREAT);

		if (shmid == -1) {
			perror("shmget");
			exit(1);
		}

		int (*data)[kolom] = shmat(shmid, (void*)0, 0);

		int transposed[kolom][baris];

		for (int i = 0; i < baris; i++) {
			for (int j = 0; j < kolom; j++) {
				transposed[j][i] = data[i][j];
			}
		}

		printf("Hasil Transpose Matriks:\n");
		for (int i = 0; i < kolom; i++) {
			for (int j = 0; j < baris; j++) {
				printf("%d ", transposed[i][j]);
			}
			printf("\n");
		}

		int hasilFaktorial[kolom][baris];

		clock_t start = clock();

		for (int i = 0; i < kolom; i++) {
			for (int j = 0; j < baris; j++) {
				hasilFaktorial[i][j] = faktorial(transposed[i][j]);
			}
		}

		clock_t end = clock();

		printf("Matriks Hasil Faktorial:\n");
		for (int i = 0; i < kolom; i++) {
			for (int j = 0; j < baris; j++) {
				printf("%d ", hasilFaktorial[i][j]);
			}
			printf("\n");
		}

		shmdt(data);

		double time_taken = ((double)(end - start)) / CLOCKS_PER_SEC;
		printf("Waktu eksekusi: %f detik\n", time_taken);

		return 0;
	}

rajin.c mirip dengan yang.c, bedanya adalah, program rajin.c tidak melakukan perhitungan faktorial dalam thread.

Program rajin.c mengambil data dari shared memory yang telah dibuat oleh belajar.c. Hasil transpose dari matriksnya disimpan dalam matriks `transposed`, kemudian setiap elemen dari matriksnya difaktorialkan. 

Durasi eksekusi program diukur menggunakan `clock()` saat sebelum dan sesudah perhitungan faktorial dilakukan. Hasil faktorial disimpan dalam matriks `hasilFaktorial`, yang kemudian program mencetak hasil faktorial serta waktu eksekusinya. Terakhir, program melepaskan shared memory menggunakan `shmdt()`.

**Output:**

![soal1](gambar/soal1.png)

Seperti yang sudah dijelaskan pada laporan, belajar.c menyimpan matriks ke shared memory, di mana yang.c dan rajin.c mengambil hasil matriksnya dari shared memory tersebut. 

Dilihat dari hasil perhitungan waktu eksekusi antara yang.c yang memakai thread dan rajin.c yang tidak menggunakan thread, program yang.c mengeksekusi perhitungan lebih lama daripada rajin.c yang tidak menggunakan thread. Hal ini dikarenakan yang.c membuat thread baru menggunakan `pthread_create()` lalu menunggu thread selesai menggunakan `pthread_join()`. Pembuatan dan penghancuran thread memiliki overhead yang tidak kecil. Untuk operasi sederhana seperti menghitung faktorial, overhead bisa lebih besar daripada manfaat pemrosesan paralel. Thread umumnya lebih berguna untuk proses yang kompleks yang dapat dipecah menjadi beberapa tugas yang bisa dijalankan secara paralel.

**Kendala:**
Perhitungan faktorialnya kacau jika angkanya terlalu besar.

**Revisi:**
Tidak ada

## Nomor 2
**Soal:**

Messi the only GOAT sedang gabut karena Inter Miami Gagal masuk Playoff. Messi menghabiskan waktu dengan mendengar lagu. Karena gabut yang tergabut gabut, Messi penasaran berapa banyak jumlah kata tertentu yang muncul pada lagu tersebut. Messi mencoba ngoding untuk mencari jawaban dari kegabutannya. Pertama-tama dia mengumpulkan beberapa lirik lagu favorit yang disimpan di file. Beberapa saat setelah mencoba ternyata ngoding tidak semudah itu, Messipun meminta tolong kepada Ronaldo, tetapi karena Ronaldo sibuk main di liga oli, Ronaldopun meminta tolong kepadamu untuk dibuatkan kode dari program Messi dengan ketentuan sebagai berikut : 
(Wajib menerapkan konsep pipes dan fork seperti yang dijelaskan di modul Sisop. Gunakan 2 pipes dengan diagram seperti di modul 3).

Messi ingin nama programnya 8ballondors.c. Pada parent process, program akan membaca file dan menghapus karakter-karakter yang bukan huruf pada file dan membuat output file dari hasil proses itu dengan nama thebeatles.txt
Pada child process, lakukan perhitungan jumlah frekuensi kemunculan sebuah kata dari output file yang akan diinputkan (kata) oleh user.

Karena gabutnya, Messi ingin program dapat mencari jumlah frekuensi kemunculan sebuah huruf dari file. Maka, pada child process lakukan perhitungan jumlah frekuensi kemunculan sebuah huruf dari output file yang akan diinputkan (huruf) user.

Karena terdapat dua perhitungan, maka pada program buatlah argumen untuk menjalankan program: 

Kata	: ./8ballondors -kata


Huruf	: ./8ballondors -huruf

Hasil dari perhitungan  jumlah frekuensi kemunculan sebuah kata dan  jumlah frekuensi kemunculan sebuah huruf dikirim ke parent process.
Messi ingin agar setiap kata atau huruf dicatat dalam sebuah log yang diberi nama frekuensi.log. Pada parent process, lakukan pembuatan file log berdasarkan data yang dikirim dari child process. 
Format: [date] [type] [message]
Type: KATA, HURUF
Ex:
[24/10/23 01:05:48] [KATA] Kata 'yang' muncul sebanyak 10 kali dalam file 'thebeatles.txt'
[24/10/23 01:04:29] [HURUF] Huruf 'a' muncul sebanyak 396 kali dalam file 'thebeatles.txt'

Catatan:
Perhitungan jumlah frekuensi kemunculan kata atau huruf  menggunakan Case-Sensitive Search. 

**Pengerjaan:**

	#include <stdio.h>
	#include <string.h>
	#include <ctype.h>
	#include <stdlib.h>
	#include <time.h>

	#define MAX_WORD_LENGTH 50

	int main(int argc, char *argv[])
	{
    FILE *input, *output,*output2;
    int count = 0;
    int count2 = 0;
    int index = 0;
    int counts[26] = {0};
    char c;
    char word[MAX_WORD_LENGTH];
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    char str[20];
    int k = 0;

    input = fopen("lirik.txt", "r");
    output = fopen("output.txt", "w");
    output2 = fopen("frekuensi.log","w");

    if (input == NULL || output == NULL || output2 == NULL)
    {
   	 printf("Error opening files.\n");
   	 return 1;
    }
    struct word_count {
   	 char word[MAX_WORD_LENGTH];
   	 int countw;
    } words[1000];

    int num_words = 0;
    char temp[MAX_WORD_LENGTH];

      if(strcmp(argv[1], "-kata") == 0) {
   	 while (fscanf(input, "%s", temp) != EOF) {
   	 int i;
   	 for (i = 0; i < num_words; i++) {
   		 if (strcmp(words[i].word, temp) == 0) {
       		 words[i].countw++;
       		 break;
   		 }
   	 }
	if (i == num_words) {
   		 strcpy(words[num_words].word, temp);
   		 words[num_words].countw = 1;
   		 num_words++;
		 }
    int d = 0;
   	 while (temp[d]) {
   		 if (isalpha(temp[d])) {
       		 fputc(temp[d],output);
   	 k++;
   		 }
      d++;
   	 }
    fputc(' ',output);

	 }
    fprintf(output2,"Words = %d\n",k);
    for (int i = 0; i < num_words; i++) {
	strftime(str, sizeof(str), "%Y/%m/%d %H:%M:%S", tm);
   	 fprintf(output2,"[%s][KATA] Kata '%s' muncul %d kali\n", str, words[i].word, words[i].countw);
    }

      }else if(strcmp(argv[1], "-huruf") == 0){
      while ((c = fgetc(input)) != EOF)
      {
 		 if (isalpha(c))
 		 {
     		 fputc(c, output);
 		 count++;
 		 word[index++] = c;
 		 }
  	else if(isspace(c))
  	{
 		 fputc(c, output);
 		 count2++;
  	}
  	else{
 		 word[index++] = c;
  	}
 		 if (c >= 'a' && c <= 'z') {
     		 counts[c - 'a']++;
 		 } else if (c >= 'A' && c <= 'Z') {
     		 counts[c - 'A']++;
 		 }
   	 }
   	 fprintf(output2,"Alphabet = %d\n",count);
	for(int i = 0;i < 26; i++) {
  	strftime(str, sizeof(str), "%Y/%m/%d %H:%M:%S", tm);
 		 fprintf(output2,"[%s][KATA] Kata '%C' muncul %d kali\n",str,'a' + i,counts[i]);
	}
      }else {
   	 fprintf(output2,"Write a correct command");
      }

    fclose(input);
    fclose(output);
    fclose(output2);
    printf("\nFile processing complete.\n");

    return 0;
	}

Kode ini menerima argumen untuk mencatat dan notifikasi jumlah kata dan alfabet.Program ini mencatat kata dengan mencari kata baru melewati membandingkan string yang ada di lirik.txt dan simpanan di program ini.Untuk menghitung huruf karena huruf lebih terbatas daripada kata huruf langsung ~~?dinonkapitalkan?~~ dikecilkan dan dimasukkan jumlah huruf dengan membandingkan jaraknya dari huruf a masukkan hasil menjadi urutan integer masalah program ini adalah:
1.argumen -huruf dan -kata itu untuk mencari suatu huruf dan kata bukan semuanya
2.program ini dari harus bekerja tanpa argumen
3.hasilnya harus ditunjukkan di_console_ terus dicatat difrekuensi.log
4.frekuensi.log harus mencatat bukan _overwrite_

**Revisi:**
	#include <stdio.h>
	#include <string.h>
	#include <ctype.h>
	#include <time.h>


	#define MAX 256


	void process(char *str) {
    int i = 0, j = 0;
    while (str[i]) {
        if (isalpha(str[i])) {
            str[j++] = tolower(str[i]);
        }
        i++;
    }
    str[j] = '\0';
	}


	int main(int argc,char *argv[]) {
    FILE *fp,*output ,*output2;
    char target[MAX];
    char target2;
    char word[MAX];
    int count = 0;
    int c;
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    char str[20];


   if(argc == 1){
    output = fopen("output.txt", "w");
    fp = fopen("lirik.txt", "r");
    if (fp == NULL) {
        printf("Failed to open file\n");
        return 1;
    }


    while ((c = fgetc(fp)) != EOF) {
        if (isalpha(c))
        {
            fputc(c, output);
        }
    else if(isspace(c))
        {
        fputc(c, output);
        }
    }
    fclose(fp);
    fclose(output);}
    else if(strcmp(argv[1], "-kata") == 0){
    strcpy(target,argv[2]);
    process(target);
    output = fopen("output.txt","w");
    output2 = fopen("frekuensi.log","a");
    fp = fopen("lirik.txt", "r");
    if (fp == NULL) {
        printf("Failed to open file\n");
        return 1;
    }


    while (fscanf(fp, "%s", word) != EOF) {
        process(word);
        if (strcmp(word, argv[2]) == 0) {
            count++;
        }
        for (int i = 0; word[i]; i++) {
            if (isalpha(word[i])) {
                fputc(tolower(word[i]), output);
            }
    }
    fputc(' ',output);
    }


    strftime(str, sizeof(str), "%Y/%m/%d %H:%M:%S", tm);
    printf("[%s][KATA] Kata '%s' muncul %d kali\n",str,target,count);
    fprintf(output2,"[%s][KATA] Kata '%s' muncul %d kali\n",str,target,count);


    fclose(fp);
    fclose(output);
    fclose(output2);
    }


    else if(strcmp(argv[1],"-huruf") == 0){
   
    output = fopen("output.txt", "w");
    target2 = tolower(argv[2][0]);
    output2 = fopen("frekuensi.log","a");
    fp = fopen("lirik.txt", "r");
    if (fp == NULL) {
        printf("Failed to open file\n");
        return 1;
    }


    while ((c = fgetc(fp)) != EOF) {
        if (isalpha(c) && tolower(c) == target2) {
            count++;
        }
        if (isalpha(c))
        {
            fputc(c, output);
        }
    else if(isspace(c))
        {
        fputc(c, output);
        }
    }
    strftime(str, sizeof(str), "%Y/%m/%d %H:%M:%S", tm);
    printf("[%s][HURUF] Kata '%c' muncul %d kali\n",str,target2,count);
    fprintf(output2,"[%s][HURUF] Kata '%c' muncul %d kali\n",str,target2,count);
    fclose(fp);
    fclose(output);
    fclose(output2);
    }
    return 0;
	}

Direvisi ini program akan mengecek apakah ada argumen atau tidak.Jika tidak program akan menulis kembali lirik dibeatles.txt huruf saja.Jika ada argumen itu akan mengecek apakah mencari huruf atau kata dan huruf kata apa.Mengganti perintah w pada fopen ke a untuk menambahkan catatan baru.Pada segi proses pencarian kata lebih dioptimisasi untuk mengilangkan non huruf penyebabnya mungkin karena efek samping mencari kata spesifik bukan semua kata sebab tidak butuh menyimpan kata baru jadi program lebih ber-optimisasi.Pada proses pencarian tidak ada yang berubah.Yang terakhir menggan ti output.txt ke thebeatles.txt

**Output:**
![soal21](gambar/Soal2_1.png)
![soal22](gambar/Soal2_2.png)
![soal23](gambar/Soal2_3.png)
![soal24](gambar/Soal2_4.png)
![soal25](gambar/Soal2_5.png)

## Nomor 3

**Soal:**
Christopher adalah seorang praktikan sisop, dia mendapat tugas dari pak Nolan untuk membuat komunikasi antar proses dengan menerapkan konsep message queue. Pak Nolan memberikan kredensial list  users yang harus masuk ke dalam program yang akan dibuat. Lebih lanjutnya pak Nolan memberikan instruksi tambahan sebagai berikut: 

Bantulah Christopher untuk membuat program tersebut, dengan menerapkan konsep message queue(wajib) maka buatlah 2  program, sender.c sebagai pengirim dan receiver.c sebagai penerima. Dalam hal ini, sender hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh receiver.
Struktur foldernya akan menjadi seperti berikut


	└── soal3
		├── users
		├── receiver.c
		└── sender.c


Ternyata list kredensial yang diberikan Pak Nolan semua passwordnya terenkripsi dengan menggunakan base64. Untuk mengetahui kredensial yang valid maka Sender pertama kali akan mengirimkan perintah CREDS kemudian sistem receiver akan melakukan decrypt/decode/konversi pada file users, lalu menampilkannya pada receiver. Hasilnya akan menjadi seperti berikut : 

	./receiver
	Username: Mayuri, Password: TuTuRuuu
	Username: Onodera, Password: K0sak!
	Username: Johan, Password: L!3b3rt
	Username: Seki, Password: Yuk!n3
	Username: Ayanokouji, Password: K!yot4kA
	….


Setelah mengetahui list kredensial, bantulah christopher untuk membuat proses autentikasi berdasarkan list kredensial yang telah disediakan. Proses autentikasi dilakukan dengan menggunakan perintah AUTH: username password kemudian jika proses autentikasi valid dan berhasil maka akan menampilkan Authentication successful . Authentication failed jika gagal.
Setelah berhasil membuat proses autentikasi, buatlah proses transfer file. Transfer file dilakukan dari direktori Sender dan dikirim ke direktori Receiver . Proses transfer dilakukan dengan menggunakan perintah TRANSFER filename . Struktur direktorinya akan menjadi seperti berikut:


	└── soal3
		├── Receiver
		├── Sender
		├── receiver.c
		└── sender.c




Karena takut memorinya penuh, Christopher memberi status size(kb) pada setiap pengiriman file yang berhasil.
Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan "UNKNOWN COMMAND".

Catatan:
Dilarang untuk overwrite file users secara permanen
Ketika proses autentikasi gagal program receiver akan mengirim status Authentication failed dan program langsung keluar
Sebelum melakukan transfer file sender harus login (melalui proses auth) terlebih dahulu
Buat transfer file agar tidak duplicate dengan file yang memang sudah ada atau sudah pernah dikirim.

**Pengerjaan:**

Berikut adalah isi dari file sender.c 

**Header:**

	#include <sys/msg.h>
	#include <stdio.h>
	#include <string.h>

Kode ini hanya mengimpor dua header file yang diperlukan untuk berinteraksi dengan antrean pesan dan untuk mengelola string.

**Struct msg_buffer:**

	struct msg_buffer {
	long msg_type;
 	char msg_text[100];
	};

Ini adalah definisi struktur data msg_buffer yang digunakan untuk menyimpan pesan yang akan dikirim melalui antrean pesan. Struktur ini memiliki dua anggota, yaitu msg_type (tipe pesan) dan msg_text (teks pesan).

**Key dan Message Queue:**

	key_t key;
	int msgid;
	key = ftok("/tmp", 65);
	msgid = msgget(key, 0666 | IPC_CREAT);

Program pertama-tama membuat sebuah kunci (key) dengan ftok yang digunakan untuk mengidentifikasi message queue yang akan digunakan.Kemudian, message queue dibuat atau dibuka dengan msgget. Jika message queue sudah ada, maka akan dibuka; jika tidak, maka akan dibuat baru dengan hak akses 0666.

**Pengiriman Pesan ke Message Queue:**

	struct msg_buffer message;
	message.msg_type = 1;
	while (1) {
   	 printf("Masukkan perintah (ketik 'exit' untuk keluar): ");
   	 fgets(message.msg_text, sizeof(message.msg_text), stdin);
   	 if (strcmp(message.msg_text, "exit\n") == 0) {
        break;
    }
    	msgsnd(msgid, &message, sizeof(message), 0);
    	printf("Perintah dikirim: %s", message.msg_text);
	}

Program membuat struktur message yang akan digunakan untuk menyimpan pesan yang akan dikirim ke antrean pesan. msg_type diatur sebagai 1.
Program memasuki loop yang akan berjalan selama program berjalan. Di dalam loop, program meminta pengguna untuk memasukkan perintah melalui fgets dan menyimpan perintah tersebut dalam message.msg_text.
Jika perintah yang dimasukkan adalah "exit\n", program keluar dari loop.
Jika perintah bukan "exit\n", program mengirimkan pesan tersebut ke antrean pesan menggunakan msgsnd.
Setelah mengirim pesan, program mencetak pesan yang dikirim ke layar.

**Berikut adalah isi dari file receiver.c**

**Headers :**

	#include <stdio.h>
	#include <sys/msg.h>
	#include <stdlib.h>
	#include <unistd.h>
	#include <sys/types.h>
	#include <sys/wait.h>
	#include <string.h>
	#include <openssl/bio.h>
	#include <openssl/evp.h>
	#include <sys/stat.h>
	#include <fcntl.h>

Ini adalah bagian awal dari kode yang mengimpor berbagai header file yang diperlukan untuk berbagai fungsi dan tipe data yang digunakan dalam program.

**Struct msg_buffer:**

	struct msg_buffer {
    long msg_type;
    char msg_text[100];
	};

Ini adalah definisi struktur data msg_buffer yang digunakan untuk menyimpan pesan yang akan dikirim dan diterima melalui antrean pesan. Struktur ini memiliki dua anggota, yaitu msg_type (tipe pesan) dan msg_text (teks pesan).

**Fungsi base64_decode:**

    BIO *bio, *b64;
    char *buffer = (char *)malloc(strlen(input));
    memset(buffer, 0, sizeof(buffer));
      
    b64 = BIO_new(BIO_f_base64());
    bio = BIO_new_mem_buf(input, -1);
    bio = BIO_push(b64, bio);
      
    BIO_read(bio, buffer, strlen(input));
    BIO_free_all(bio);
      
    return buffer;
	}
Ini adalah definisi fungsi base64_decode yang digunakan untuk mendekode string yang dienkripsi dengan base64. Fungsi ini menggunakan pustaka OpenSSL untuk dekripsi base64 dan mengembalikan string yang didekodekan.


**Key dan Message Queue:**

    key_t key;
	int msgid;
	key = ftok("/tmp", 65);
	msgid = msgget(key, 0666 | IPC_CREAT);

Program pertama-tama membuat sebuah kunci (key) dengan ftok yang digunakan untuk mengidentifikasi message queue.Kemudian, message queue dibuat dengan msgget. Jika message queue sudah ada, maka akan dibuka; jika tidak, maka akan dibuat baru.

**Pesan dan Loop Utama:**

	struct msg_buffer message;
    while (1) {
        msgrcv(msgid, &message, sizeof(message), 1, 0);

        printf("Menerima perintah: %s", message.msg_text);

        if (strcmp(message.msg_text, "exit\n") == 0) {
            break;
        }

Program memasukkan pesan yang diterima dari message queue ke dalam struktur message dan mencetak pesan tersebut.
Program memasuki loop utama yang akan berjalan selama program berjalan. Loop ini digunakan untuk menerima dan memproses pesan.

**Proses membaca Kredensial**

  	if (strcmp(message.msg_text, "CREDS\n") == 0) {
        FILE *file = fopen("users/users.txt", "r");
            if (file != NULL) {
                char line[100];

                while (fgets(line, sizeof(line), file)) {
                    char *username = strtok(line, ":");
                    char *password = strtok(NULL, ":");

                    char *decodedPassword = base64_decode(password);

                    if (decodedPassword != NULL) {
                        printf("Username: %s", username);
                        printf("Password: %s\n", decodedPassword);

                        free(decodedPassword);
                    }
                }
                fclose(file);
            }
kode ini bertujuan untuk membaca kredensial pengguna dari file "users/users.txt", mendekode password yang dienkripsi dengan base64, dan mencetaknya ke layar untuk tujuan otentikasi.

**Proses Autentikasi**

	else if (strncmp(message.msg_text, "AUTH:", 5) == 0) {
        char *credentials = message.msg_text + 5;
        char *username = strtok(credentials, " ");
        char *password = strtok(NULL, " ");


            FILE *file = fopen("users/users.txt", "r");
            if (file != NULL) {
                char line[100];

                while (fgets(line, sizeof(line), file)) {
                    char *fileUsername = strtok(line, ":");
                    char *filePassword = strtok(NULL, ":");

                    char *decodedPassword = base64_decode(filePassword);

                    if (decodedPassword != NULL) {
                        if (strcmp(fileUsername, username) == 0 && strcmp(decodedPassword, password) == 0) {
                            printf("Otentikasi berhasil.\n");
                            free(decodedPassword);
                            break;
                        }
                        free(decodedPassword);
                    }
                }
                fclose(file);
            }

kode ini bertujuan untuk memeriksa otentikasi pengguna berdasarkan kredensial yang diberikan dalam pesan "AUTH". Jika kredensial sesuai dengan yang ada dalam file "users/users.txt", otentikasi dianggap berhasil.

**Proses Transfer file** 

	else if (strncmp(message.msg_text, "TRANSFER ", 9) == 0) {
    char *filename = message.msg_text + 9; 
            

    char sourcePath[100];
    char destinationPath[100];
    sprintf(sourcePath, "Sender/%s", filename);
    sprintf(destinationPath, "Receiver/%s", filename);

    FILE *sourceFile = fopen(sourcePath, "rb");
    if (sourceFile != NULL) {
    FILE *destinationFile = fopen(destinationPath, "wb");
    if (destinationFile != NULL) {
    char buffer[1024];
    size_t bytesRead;

    while ((bytesRead = fread(buffer, 1, sizeof(buffer), sourceFile)) > 0) {
    fwrite(buffer, 1, bytesRead, destinationFile);
        }

    fclose(destinationFile);
    } else {
    printf("Gagal membuat file tujuan.\n");
    }

    fclose(sourceFile);
        } else {
                printf("File sumber tidak ditemukan.\n");
            }
        } else {

            int status = system(message.msg_text);

            if (WIFEXITED(status)) {
                printf("Perintah selesai dengan status keluaran: %d\n", WEXITSTATUS(status));
            }
        }
    }

kode ini adalah bagian dari program yang menangani transfer file jika pesan yang diterima dimulai dengan "TRANSFER". Program akan mencoba menyalin file dari direktori sumber ("Sender") ke direktori tujuan ("Receiver"). Jika pesan tidak sesuai dengan "TRANSFER", program akan menjalankan perintah sistem lainnya.

**Penghapusan Message Queue dan Selesai:**

	msgctl(msgid, IPC_RMID, NULL);
	return 0;

Setelah selesai, program menghapus message queue dengan msgctl dan mengakhiri eksekusi.

**Output:**

Sebelum masuk ke percobaan fungsi, pertama tama kita lihat terlebih dahulu isi dari file yang ada di device 

![soal4](gambar/Soal4-1.png)

Seperti yang dilihat, semua isi file sudah sesuai dengan apa yang soal minta

Lanjut Pada pembahasan fungsi, pertama kita diminta untuk membuat fungsi CREDS

![soal4](gambar/Soal4-2.png)

Seperti yang terlihat pada gambar, fungsi CREDS dapat berjalan dengan baik, namun terdapat 1 password yang tidak muncul

Kemudian yang kedua pada soal, kita diminta untuk membuat fungsi AUTH

![soal4](gambar/Soal4-3.png)

Seperti yang bisa dilihat pada gambar fungsi AUTH masih belum terimplementasi dengan benar, dikarenakan pada saat password dan username dicoba, AUTH tidak bisa mengotentikasi input tersebut

Yang ketiga, kita diminta untuk membuat Transfer file

![soal4](gambar/Soal4-4.png)

Seperti yang terlihat pada gambar Proses Transfer file juga masih belum dapat dilakukan, sepertinya dikarenakan program tidak dapat membaca isi dari file tersebut

**Kendala:**
Pada saat ini kendala yang sedang dihadapi ialah program receiver belum bisa melakukan AUTH dan TRANSFER dengan benar

## Nomor 4
**Soal:**

Takumi adalah seorang pekerja magang di perusahaan Evil Corp. Dia mendapatkan tugas untuk membuat sebuah public room chat menggunakan konsep socket. Ketentuan lebih lengkapnya adalah sebagai berikut:

Client dan server terhubung melalui socket. 
Server berfungsi sebagai penerima pesan dari client dan hanya menampilkan pesan saja.  
karena resource Evil Corp sedang terbatas buatlah agar server hanya bisa membuat koneksi dengan 5 client saja.

**Pengerjaan:**

Berikut adalah isi dari file server.c:
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <unistd.h>
	#include <arpa/inet.h>
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <pthread.h>

	#define MAX_CLIENTS 5

	void* handle_client(void* socket_desc) {
		int socket_client = *(int*)socket_desc;
		char pesan[1024];
		int koneksi;

		while (1) {
			koneksi = recv(socket_client, pesan, sizeof(pesan), 0);
			if (koneksi == -1) {
				perror("Gagal Menerima");
				break;
			}

			if (koneksi == 0) {
				printf("Client telah menutup koneksi.\n");
				break;
			}

			pesan[koneksi] = '\0';
			printf("Client: %s\n", pesan);
		}

		close(socket_client);
		free(socket_desc);
		pthread_exit(NULL);
	}

	int main() {
		int socket_server, socket_client;
		int jumlah_client = 0;
		pthread_t threads[MAX_CLIENTS];

		socket_server = socket(AF_INET, SOCK_STREAM, 0);
		if (socket_server == -1) {
			perror("Gagal membuat socket.");
			exit(1);
		}

		struct sockaddr_in server_address;
		server_address.sin_family = AF_INET;
		server_address.sin_port = htons(8080);
		server_address.sin_addr.s_addr = INADDR_ANY;

		if (bind(socket_server, (struct sockaddr*)&server_address, sizeof(server_address)) == -1) {
			perror("Koneksi gagal");
			exit(1);
		}

		if (listen(socket_server, 5) == -1) {
			perror("Gagal menghubungkan");
			exit(1);
		}

		printf("Server sedang menunggu koneksi...\n");

		struct sockaddr_in client_address;
		socklen_t client_address_len = sizeof(client_address);

		while (jumlah_client < MAX_CLIENTS) {
			socket_client = accept(socket_server, (struct sockaddr*)&client_address, &client_address_len);

			if (socket_client == -1) {
				perror("Gagal Menerima");
				exit(1);
			}

			printf("Terhubung dengan client %d\n", jumlah_client + 1);

			int* new_socket = malloc(sizeof(int));
			*new_socket = socket_client;

			if (pthread_create(&threads[jumlah_client], NULL, handle_client, (void*)new_socket) != 0) {
				perror("Gagal membuat thread");
				free(new_socket);
			}

			jumlah_client++;
		}

		for (int i = 0; i < jumlah_client; i++) {
			pthread_join(threads[i], NULL);
		}

		close(socket_server);
		return 0;
	}

server berfungsi sebagai penerima pesan dari client. server menggunakan fungsi `socket()` untuk membuat socket dan mengikatnya ke alamat dan port tertentu menggunakan `bind()`.

server mendengarkan koneksi yang masuk dengan `listen()` dan menerima koneksi client yang masuk menggunakan `accept()`. Koneksi client diterima dalam loop, dan setiap koneksi dikelola dalam thread terpisah.

Fungsi `handle_client()` dijalankan dalam thread terpisah untuk menangani setiap koneksi client. Server menerima pesan dari client dengan `recv()`, lalu menampilkan pesan tersebut dan menutup koneksi client saat diperlukan, seperti jumlah client melebihi batas maksimal atau client memang menutup koneksinya dengan `ctrl+c`. Jumlah maksimal client yang dapat terhubung berbarengan ditentukan oleh `MAX_CLIENTS`.

Berikut adalah isi dari file client.c:

	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <unistd.h>
	#include <arpa/inet.h>
	#include <sys/socket.h>

	int main() {
		int socket_client; 
		char pesan[1024];

		socket_client = socket(AF_INET, SOCK_STREAM, 0);
		if (socket_client == -1) {
			perror("Gagal membuat socket.");
			exit(1);
		}

		struct sockaddr_in server_address; 
		server_address.sin_family = AF_INET;
		server_address.sin_port = htons(8080);
		server_address.sin_addr.s_addr = INADDR_ANY;

		if (connect(socket_client, (struct sockaddr*)&server_address, sizeof(server_address)) == -1) {
			perror("Gagal menghubungkan.");
			exit(1);
		}

		while (1) {
			printf("Masukkan pesan: ");
			fgets(pesan, sizeof(pesan), stdin);
			send(socket_client, pesan, strlen(pesan), 0);
		}

		close(socket_client);
		return 0;
	}

client berfungsi untuk mengirim pesan ke server menggunakan `socket()` yang berguna untuk membuat socket dan menghubungkan ke alamat dan port yang sama yang digunakan oleh server menghubungkan `connect()`.

Jika sudah terhubung, client memasukkan pesan menggunakan `fgets()` dan mengirimnya ke server dengan `send()`. Loop ini memungkinkan pengguna untuk terus-menerus mengirim pesan ke server.

**Output:**

![soal4](gambar/soal4.png)

Server dapat terhubung dengan 5 client dan menerima pesan dari 5 client sekaligus.

**Kendala:**
client tidak bisa berjalan berbarengan, namun sudah direvisi dan sekarang client bisa berjalan berbarengan maksimal sebanyak 5 client. Jika lebih dari 5, makan server tidak akan menerima pesan dari client ke-6 dan seterusnya.

**Revisi:**
server.c direvisi agar bisa menerima pesan dari 5 client berbarengan. 
