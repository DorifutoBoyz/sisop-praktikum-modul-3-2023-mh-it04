#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>

#define MAX_WORD_LENGTH 50

int main(int argc, char *argv[])
{
    FILE *input, *output,*output2;
    int count = 0;
    int count2 = 0;
    int index = 0;
    int counts[26] = {0};
    char c;
    char word[MAX_WORD_LENGTH];
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    char str[20];
    int k = 0;

    input = fopen("lirik.txt", "r");
    output = fopen("output.txt", "w");
    output2 = fopen("frekuensi.log","w");

    if (input == NULL || output == NULL || output2 == NULL)
    {
   	 printf("Error opening files.\n");
   	 return 1;
    }
    struct word_count {
   	 char word[MAX_WORD_LENGTH];
   	 int countw;
    } words[1000];

    int num_words = 0;
    char temp[MAX_WORD_LENGTH];

      if(strcmp(argv[1], "-kata") == 0) {
   	 while (fscanf(input, "%s", temp) != EOF) {
   	 int i;
   	 for (i = 0; i < num_words; i++) {
   		 if (strcmp(words[i].word, temp) == 0) {
       		 words[i].countw++;
       		 break;
   		 }
   	 }
	if (i == num_words) {
   		 strcpy(words[num_words].word, temp);
   		 words[num_words].countw = 1;
   		 num_words++;
		 }
    int d = 0;
   	 while (temp[d]) {
   		 if (isalpha(temp[d])) {
       		 fputc(temp[d],output);
   	 k++;
   		 }
      d++;
   	 }
    fputc(' ',output);

	 }
    fprintf(output2,"Words = %d\n",k);
    for (int i = 0; i < num_words; i++) {
	strftime(str, sizeof(str), "%Y/%m/%d %H:%M:%S", tm);
   	 fprintf(output2,"[%s][KATA] Kata '%s' muncul %d kali\n", str, words[i].word, words[i].countw);
    }

      }else if(strcmp(argv[1], "-huruf") == 0){
      while ((c = fgetc(input)) != EOF)
      {
 		 if (isalpha(c))
 		 {
     		 fputc(c, output);
 		 count++;
 		 word[index++] = c;
 		 }
  	else if(isspace(c))
  	{
 		 fputc(c, output);
 		 count2++;
  	}
  	else{
 		 word[index++] = c;
  	}
 		 if (c >= 'a' && c <= 'z') {
     		 counts[c - 'a']++;
 		 } else if (c >= 'A' && c <= 'Z') {
     		 counts[c - 'A']++;
 		 }
   	 }
   	 fprintf(output2,"Alphabet = %d\n",count);
	for(int i = 0;i < 26; i++) {
  	strftime(str, sizeof(str), "%Y/%m/%d %H:%M:%S", tm);
 		 fprintf(output2,"[%s][KATA] Kata '%C' muncul %d kali\n",str,'a' + i,counts[i]);
	}
      }else {
   	 fprintf(output2,"Write a correct command");
      }

    fclose(input);
    fclose(output);
    fclose(output2);
    printf("\nFile processing complete.\n");

    return 0;
}



