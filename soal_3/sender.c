#include <stdio.h>
#include <sys/msg.h>
#include <string.h>

struct msg_buffer {
    long msg_type;
    char msg_text[100];
};

int main() {
    key_t key;
    int msgid;

    // Mendapatkan kunci unik
    key = ftok("/tmp", 65);

    // Membuat message queue
    msgid = msgget(key, 0666 | IPC_CREAT);

    struct msg_buffer message;

    // Mengatur tipe pesan
    message.msg_type = 1;

    while (1) {
        printf("Masukkan perintah (ketik 'exit' untuk keluar): ");
        fgets(message.msg_text, sizeof(message.msg_text), stdin);

        if (strcmp(message.msg_text, "exit\n") == 0) {
            break; // Keluar dari loop jika pesan adalah 'exit'
        }

        // Mengirim perintah ke message queue
        msgsnd(msgid, &message, sizeof(message), 0);

        printf("Perintah dikirim: %s", message.msg_text);
    }

    return 0;
}

