#include <stdio.h>
#include <sys/msg.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <openssl/bio.h>
#include <openssl/evp.h>

struct msg_buffer {
    long msg_type;
    char msg_text[100];
};

// Deklarasi fungsi untuk mendekode Base64
char* base64_decode(const char* input);

// Fungsi untuk melakukan autentikasi
int authenticate(char* username, char* password);

int main() {
    key_t key;
    int msgid;

    // Mendapatkan kunci unik
    key = ftok("/tmp", 65);

    // Mendapatkan ID message queue
    msgid = msgget(key, 0666 | IPC_CREAT);

    struct msg_buffer message;

    // Tetap berjalan hingga Anda menekan Ctrl + C
    while (1) {
        // Menerima perintah dari message queue
        msgrcv(msgid, &message, sizeof(message), 1, 0);

        printf("Menerima perintah: %s", message.msg_text);

        if (strcmp(message.msg_text, "exit\n") == 0) {
            break; // Keluar dari loop jika pesan adalah 'exit'
        }

        // Memeriksa jika perintah adalah "CREDS"
        if (strcmp(message.msg_text, "CREDS\n") == 0) {
            // Membaca kredensial dari file users/users.txt
            FILE *file = fopen("users/users.txt", "r");
            if (file != NULL) {
                char line[100];

                while (fgets(line, sizeof(line), file)) {
                    char *username = strtok(line, ":");
                    char *password = strtok(NULL, ":");

                    // Mendekode nilai password jika ada
                    char *decodedPassword = base64_decode(password);

                    if (decodedPassword != NULL) {
                        printf("Username: %s", username);
                        printf("Password: %s\n", decodedPassword);

                        free(decodedPassword);
                    }
                }
                fclose(file);
            }
        } else if (strncmp(message.msg_text, "AUTH:", 5) == 0) {
            char* authStr = message.msg_text + 5;
            char* username = strtok(authStr, " ");
            char* password = strtok(NULL, " ");
            int result = authenticate(username, password);

            if (result == 1) {
                printf("Authentication successful\n");
            } else {
                printf("Authentication failed\n");
            }
        } else {
            // Eksekusi perintah menggunakan system()
            int status = system(message.msg_text);

            if (WIFEXITED(status)) {
                printf("Perintah selesai dengan status keluaran: %d\n", WEXITSTATUS(status));
            }
        }
    }

    // Menghapus message queue
    msgctl(msgid, IPC_RMID, NULL);

    return 0;
}

// Fungsi untuk mendekode Base64
char* base64_decode(const char* input) {
    BIO *bio, *b64;
    char *buffer = (char *)malloc(strlen(input));
    memset(buffer, 0, sizeof(buffer));

    b64 = BIO_new(BIO_f_base64());
    bio = BIO_new_mem_buf(input, -1);
    bio = BIO_push(b64, bio);

    BIO_read(bio, buffer, strlen(input));
    BIO_free_all(bio);

    return buffer;
}

// Fungsi untuk melakukan autentikasi
int authenticate(char* username, char* password) {
    FILE *file = fopen("users/users.txt", "r");
    if (file != NULL) {
        char line[100];

        while (fgets(line, sizeof(line), file)) {
            char *storedUsername = strtok(line, ":");
            char *storedPasswordBase64 = strtok(NULL, ":");

            // Decode stored password from Base64
            char *decodedPassword = base64_decode(storedPasswordBase64);

            if (decodedPassword != NULL && strcmp(username, storedUsername) == 0 && strcmp(password, decodedPassword) == 0) {
                free(decodedPassword);
                fclose(file);
                return 1; // Authentication successful
            }

            if (decodedPassword != NULL) {
                free(decodedPassword);
            }
        }
        fclose(file);
    }

    return 0; // Authentication failed
}

